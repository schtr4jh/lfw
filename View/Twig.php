<?php

namespace LFW\View;

use LFW\Settings;

class Twig extends \LFW\View {
	private $twig;

	function __construct($view = null, $data = array()) {
		$arrTwigLoaderFilesystem = array();

		$arrTwigLoaderFilesystem[] =  APP_SRC_PATH;

		if (substr($view, 0, 1) != "/") { // add namespace
			$db = debug_backtrace();

			foreach (array(0,1,2,3) AS $i) { // @T00D00 - make this prettier
				if (!isset($db[$i]["file"])) {
					continue;
				}

				foreach (array(
					str_replace(APP_SRC_PATH, "", $db[$i]["file"]),
					str_replace(SRC_PATH, "", $db[$i]["file"]),
					) AS $file) {
					foreach (array(
						APP_SRC_PATH . substr($file, 0, strpos($file, "/Controller/")) . "/views/",
						SRC_PATH . substr($file, 0, strpos($file, "/Controller/")) . "/views/",
					) AS $dir) {
						if (!in_array($dir, $arrTwigLoaderFilesystem) && is_dir($dir))
							$arrTwigLoaderFilesystem[] = $dir;
					}
				}
			}
		}
		$arrTwigLoaderFilesystem[] =  SRC_PATH;

		$this->twig = new \Twig_Environment(new \Twig_Loader_Chain(array(
				new \Twig_Loader_Filesystem($arrTwigLoaderFilesystem),
				new \Twig_Loader_String())
			),
			array(
				'debug' => true
			)
		);

		if (true || Settings::get("dev"))
			$this->twig->addExtension(new \Twig_Extension_Debug());

		$this->twig->addExtension(new \Twig_Extension_StringLoader());

		$this->twig = $this->twig->loadTemplate($view . ".twig");

		$this->view = $this;
		$this->data = $data;

		foreach (self::$globals AS $key => $val) {
			$this->data[$key] = $val;
		}
	}

	function autoparse() {
		foreach (array(
					"_optimize" => \LFW\Helpers\Optimize::getHTML(),
					//"_seo" => \LFW\Helpers\SEO::getHTML(),
					"_debug" => \LFW\Debug::getSession(),
				) AS $key => $val) {
			$this->data[$key] = $val;
		}

		$config = Settings::get("_lfw_config");
		if (isset($config['defaults']['twig']['classes']))
		foreach ($config['defaults']['twig']['classes'] AS $key => $class) {
			$this->data[$key] = new $class;
		}
		
		try {
			return $this->twig->render($this->data);
		} catch (\Twig_Error_Syntax $e) {
			return "<pre>Twig error:" . print_r($e, TRUE). "</pre>";
		}

		return Settings::get("dev") ? "Cannot parse view '" . $this->view . "'" : NULL;
	}

	function __toString() {
		return $this->autoparse();
	}
}

?>