<?php

namespace LFW;

class Request extends Lazy {
	const GET = 1;
	const POST = 2;
	const PUT = 3;
	const DELETE = 4;

	private $_post = array();
	private $_get = array();

	function __construct() {
		// set method
	}

	function getPost($key = NULL) {
		return is_null($key) ? new Lazy($_POST) : (isset($_POST[$key]) ? $_POST[$key] : FALSE);
	}

	function getGet($key = NULL) {
		return is_null($key) ? new Lazy($_GET) : (isset($_GET[$key]) ? $_GET[$key] : FALSE);
	}

	function getFiles($key = NULL) {
		return is_null($key) ? new Lazy($_FILES) : (isset($_FILES[$key]) ? $_FILES[$key] : FALSE);
	}

	function getSession($key = NULL) {
		return is_null($key) ? new Lazy($_SESSION) : (isset($_SESSION[$key]) ? $_SESSION[$key] : FALSE);
	}

	function isMethod($method) {
		return ($method == self::GET && $_SERVER['REQUEST_METHOD'] == "GET")
			|| ($method == self::POST && $_SERVER['REQUEST_METHOD'] == "POST")
			|| ($method == self::PUT && $_SERVER['REQUEST_METHOD'] == "PUT")
			|| ($method == self::DELETE && $_SERVER['REQUEST_METHOD'] == "DELETE");
	}

	function isAjax() {
		return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') || isset($_POST['ajax']);
	}

	function isPost() {
		return $this->isMethod(self::POST);
	}

	function isGet() {
		return $this->isMethod(self::GET);
	}

	function host() {
		return $_SERVER['HTTP_HOST'];
	}

	function scheme() {
		return $_SERVER['REQUEST_SCHEME'];
	}

	function uri() {
		return $_SERVER['REQUEST_URI'];
	}

	function prodUri() {
		return str_replace("/dev.php/", "/", $_SERVER['REQUEST_URI']);
	}
}

?>