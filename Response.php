<?php

namespace LFW;

class Response {
	const HTML = 1;
	const JSON = 2;

	private $type;
	private $output;

	public function __construct($output = null, $type = self::HTML) {
		$this->output = $output;
		$this->type = $type;
	}

	public function setType($type) {
		$this->type = $type;
	}

	public function setOutput($output) {
		$this->output = $output;
	}

	public function getOutput() {
		return $this->output;
	}

	public function getHeaders() {
		$headers = array();
		if ($this->type == self::JSON) {
			return json_encode($this->output);
		} else if ($this->type == self::HTML) {
			return $this->output;
		}

		return $this->output;
	}

	public function output() {
		echo $this->getOutput();
	}
}