<?php

namespace LFW\Record;

class Collection implements \Iterator {
	protected $collection = array();

	public function __construct($array, $encapsulate = NULL) {
		if ($encapsulate) {

		}

    	$this->collection = $array;
	}

	public function rewind() {
		return reset($this->collection);
	}

	public function current() {
		return current($this->collection);
	}

	public function key() {
		return key($this->collection);
	}

	public function next() {
		return next($this->collection);
	}

	public function valid() {
		return key($this->collection) !== null;
	}

	public function organizeByID() {
		$new = array();
		foreach ($this->collection AS $row) {
			$new[$row->getID()] = $row;
		}
		$this->collection = $new;
		return $this->collection;
	}

	public function getList() {
		$return = array();
		
		foreach ($this->collection AS $i => $row) {
			if (!is_array($row)) {
				$row = $row->__toArray();
			}

			foreach (array("title", "slug", "name", "email", "key", "id") AS $key) {
				if (isset($row[$key])) {
					$return[] = $row[$key];
					break;
				}
			}
		}

		return $return;
	}

	public function getListID() {
		$return = array();

		foreach ($this->collection AS $i => $row) {
			if (!is_array($row)) {
				$row = $row->__toArray();
			}

			foreach (array("title", "slug", "name", "email", "key", "id") AS $key) {
				if (isset($row[$key])) {
					$return[$row['id']] = $row[$key];
					break;
				}
			}
		}

		return $return;
	}

	public function __toArray() {
		return $this->collection;
	}
}