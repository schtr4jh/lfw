<?php

namespace LFW;

class Cache {
  private static $cache;
  private static $instances;
  protected $data;

  function __construct($cache) {
    self::$instances[] = $cache;
    self::$cache = $cache;
  }

  function get($key, $default = null) {
    return self::$cache->get($key, $default);
  }

  function set($key, $value, $time = "1 hour") {
    return self::$cache->set($key, $value, $time);
  }

  function exists($key) {
    return self::$cache->exists($key);
  }

  function delete($key) {
    return self::$cache->delete($key);
  }
}

?>