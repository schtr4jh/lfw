<?php

namespace LFW\DatabaseInterface;

use LFW\DatabaseInterface;
use LFW\Context;

class PDO implements DatabaseInterface {
  function __construct($settings) {
    try {
      $pdo = new \PDO($settings["driver"] . ":host=" . $settings["host"] . ";dbname=" . $settings["db"] . ";charset=" . $settings["charset"], $settings["user"], $settings["pass"]);
      $pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ);
      $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

      Context::setDB($pdo);
    } catch (\PDOException $e) {
      throw new Exception\Runtime("Cannot connect to database", 1, $e);
    }
  }

  public static function inst() {
    return Context::getDB();
  }
}