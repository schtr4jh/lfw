<?php

namespace LFW;

class Lazy {
	protected $data = array();

	function __construct($arr = array()) {
		if (is_array($arr)) {
			foreach ($arr AS $key => $val) {
				//if (is_array($val)) {
				//	$this->__set($key, new Lazy($val));
				//} else {
					$this->__set($key, $val);
				//}
			}
		}
	}
	function __call($name, $args) {
		if (isset($this->data[$name]) && is_callable($this->data[$name]))
			return call_user_method_array($data[$name], $this, $args);
		else if (substr($name, 0, 3) == "get" && $this->__isset(lcfirst(substr($name, 3))))
			return $this->__get(substr($name, 3));
		else if (substr($name, 0, 3) == "set" && count($args) == 1)
			return $this->__set(substr($name, 3), $args);
	}

	function __get($name) {
		if (!$this->__isset($name)) return NULL;

		return $this->data[$name];
	}

	function __set($name, $val) {
		$this->data[$name] = $val;
		return $this;
	}

	function __isset($name) {
		return isset($this->data[$name]);
	}

	function __toInt() {
		return 0;
	}

	function __toString() {
		return (string)$this->data;
	}

	function __toArray() {
		$arr = array();
		foreach ($this->data AS $key => $val) {
			$arr[$key] = is_object($val) ? $val->__toArray() : $val;
		}
		return $arr;
	}

	function get($name, $default = NULL) {
		return $this->__isset($name) ? $this->__get($name) : $default;
	}
}

?>