<?php

namespace LFW;

class Context {
	// "singletons"
	protected static $request = null, $response = null, $router = null, $db = null;
	protected static $custom;

	// app speciffics
	protected static $controller = array(), $entity = array(), $record = array(), $form = array();

	function __construct() {
		self::restart();
	}

	public static function restart() {
		self::$request = new Request();
		self::$response = new Response();
		self::$router = new Router();

		self::$controller = array();
		self::$entity = array();
		self::$record = array();
		self::$form = array();
	}

	public static function getCore() {
		return Core::getCore();
	}

	public static function get($class, $cache = TRUE) {
		if (!$class) {
			throw new Exception\Input("Missing class paramater");
		}

		$sha1Class = sha1($class);

		if ($cache && isset(self::$custom[$sha1Class])) {
			return self::$custom[sha1($class)];
		}

	    if (class_exists($class)) {
	      return $self::$custom[$sha1Class] = new $class;
	    }

		throw new Exception\Runtime("Cannot get " . $class);
	}

	public static function set($key, $val) {
		self::$custom[sha1($key)] = $val;
	}

	// singletons
	public static function getRequest() {
		return self::$request;
	}

	public static function getResponse() {
		return self::$response;
	}

	public static function getRouter() {
		return self::$router;
	}

	public static function getSession() {
		return self::getRequest()->getSession();
	}

	// database
	public static function setDB($db) {
		self::$db = $db;
	}

	public static function getDB() {
		return self::$db;
	}

	// by package
	private static function name($wantedType, $calledClass) {
		if (in_array($wantedType, array('Controller', 'Entity', 'Form'))) {
			return Generator::nameMultiple(str_replace(
				array("\Controller\\", "\Entity\\", "\Form\\"),
				"\\" . $wantedType . "\\",
				$calledClass
			));
		} else if (in_array($wantedType, array('Record'))) {
			return Generator::nameOne(str_replace(
				array("\Record\\"),
				"\\" . $wantedType . "\\",
				$calledClass
			));
		}

		throw new Exception\Runtime("'" . $wantedType . "' is not registered in LFW\Context");
	}

	private static function tryLoad($wantedType, $wantedClass = null, $cache = true) {
		if (!$wantedClass) {
			$db = debug_backtrace();
			$wantedClass = self::name($wantedType, get_class($db[2]['object']));
		}

	    $wantedTypeLower = strtolower($wantedType);
	    $sha1WantedClass = "x" . sha1($wantedClass);
	    $arrCache = self::$$wantedTypeLower;

	    if ($cache && array_key_exists($sha1WantedClass, $arrCache)) {
	    	return $arrCache[$sha1WantedClass];
	    }

	    if (class_exists($wantedClass)) {
	      return $arrCache[$sha1WantedClass] = new $wantedClass;
	    }

	    throw new Exception\NotFound($wantedType . " " . $wantedClass . " not found.");
	}

	public static function getController($controller = null, $cache = true) {
		return self::tryLoad('Controller', $controller, $cache);
	}

	public static function getEntity($entity = null, $cache = true) {
		return self::tryLoad('Entity', $entity, $cache);
	}

	public static function getRecord($record = null, $cache = true) {
		return self::tryLoad('Record', $record, $cache);
	}

	public static function getForm($form = null, $cache = true) {
		return self::tryLoad('Form', $form, $cache);
	}
}