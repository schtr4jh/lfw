<?php

namespace LFW;

interface DatabaseInterface {
  function __construct($settings);
  static function inst();
}