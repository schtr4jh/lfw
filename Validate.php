<?php

namespace LFW;
// If (Validate::isInt($var)) print "is int"; else "is not int";

class Validate {
	public static function isInt($int) {
    	return $int === (int)$int || ctype_digit($int);
    }
    
    public static function isFloat($float) {
    	return $float === (float)$float || is_float($float);
    }
    
    public static function isString($string) {
    	return $string === (string)$string || is_string($string) || is_null($string);
    }
    
    public static function isNumeric($num) {
    	return ctype_digit($num);
    }
    
    public static function isAlphaNumeric($alphanum) {
    	return ctype_alnum($alphanum);
    }
    
    public static function isBool($bool) {
    	return is_bool($bool);
    }
    
    public static function fromBool($bool, $check = FALSE) {
    	if (self::isBool($bool)) {
    		$arrayCheck[1] = array("yes", "y", TRUE, 1);
    		$arrayCheck[0] = array("no", "n", FALSE, 0, -1);
    		
    		foreach ($arrayCheck AS $retBool => $values) {
    			foreach ($values AS $value) {
    				if ($value === $bool) {
    					return $retBool;
    				}
    			}
    		}
    	}
    	
    	return $check == TRUE ? FALSE : $bool;
    }
    
    public static function isMail($mail) {
    	return filter_var($mail, FILTER_VALIDATE_EMAIL) != FALSE;
    }
    
    public static function isUrl($url) {
    	return filter_var($url, FILTER_VALIDATE_URL);
    }
    
    public static function isIP($ip) {
    	return filter_var($ip, FILTER_VALIDATE_IP);
    }
    
    public static function isDatetime($date) {
    	return strtotime($date) > 0 ? TRUE : FALSE;
    }
	
	public static function isNullDate($date) {
		return empty($date) || is_null($date) || $date == "0000-00-00 00:00:00" || $date == "0000-00-00" || $date == "1970-01-01 00:00:00" || $date == "1970-01-01";
	}
	
	public static function isEmpty($arr, $keys) {
		if (empty($arr))
			return TRUE;
		
		if (is_array($arr))
		foreach ($keys AS $key) {
			if (!isset($arr[$key]) || empty($arr[$key]))
				return TRUE;
		}
		
		return FALSE;
	}

    public static function isHash($hash, $type = "sha1") {
        return
            ($type == "sha1" && preg_match('/^[0-9a-f]{40}$/i', $hash))
            ||
            ($type == "md5" && preg_match('/^[0-9a-f]{32}$/i', $hash));
    }
}

?>