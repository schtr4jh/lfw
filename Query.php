<?php

namespace LFW;

class Query {
	protected $table, $join, $where, $groupBy, $having, $orderBy, $limit;

	function __construct() {
	}

	function setTable($table) {
		$this->table = $table;
		return $this;
	}

	function buildJoin() {
		return implode(" ", $this->join);
	}

	function buildWhere() {
		return $this->where ? ' WHERE ' . implode(" AND ", $this->where) : '';
	}

	function buildHaving() {
		return $this->having ? ' HAVING ' . implode(" AND ", $this->having) : '';
	}

	// setters
	function setJoin($join) {
		$this->join = $join;
		return $this;
	}

	function setWhere($where) {
		$this->where = $where;
		return $this;
	}

	function setOrderBy($orderBy) {
		$this->orderBy = $orderBy;
		return $this;
	}

	function setLimit($limit) {
		$this->limit = $limit;
		return $this;
	}

	// adders
	function addWhere($where) {
		$this->where[] = $where;
		return $this;
	}

	function addJoin($join) {
		$this->join[] = $join;
		return $this;
	}
}