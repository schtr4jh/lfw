<?php

namespace LFW;

class Session extends Lazy {
	function __construct(&$_session = array()) {
		if (empty($_session)) $_session = $_SESSION;

		parent::__construct($_session);
	}
}

?>