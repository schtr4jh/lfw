<?php

namespace LFW;

class Record {
	protected $id;
	protected $_entity;
	public $_forced = array();
	protected $_changed = array();

	function __construct($data = array()) {
		$this->setArray($data, FALSE);
	}

	function reconstruct() {
		$this->_forced = array();
		$this->_changed = array();
	}

	function forceSet($data) {
		$this->setArray($data, TRUE);

		return $this;
	}

	function emptyChanged() {
		$this->_changed = array();

		return $this;
	}

	function validate() {
		return TRUE;
	}

	function validateOrException() {
		if (!$this->validate) throw new Exception("Invalid data (validateOrException).");

		return $this;
	}

	function existsOrException() {
		if (!is_int($this->getId())) throw new Exception("Record doesn't exist.");

		return $this;
	}

	function setArray($data, $force = FALSE, $fromDB = FALSE) {
		if (is_null($data))
			return $this;

		if (!is_array($data))
			$data = (array)$data;

		foreach ($data AS $key => $val) {
			if (property_exists($this, $key)) {
				$this->{$key} = $val;
				if (!$fromDB)
				$this->_changed[$key] = $key;
			} else if ($force) {
				$this->_forced[$key] = $val;
			}
		}

		return $this;
	}

	public function set($key, $value = null) {
		if ($this->{$key} !== $value) {
			$this->{$key} = $value;
			$this->_changed[$key] = $key;
		}

		return $this;
	}

	function get($key) {
		return $this->{$key};
	}

	public function setUnchanged($key, $value) {
		$this->{$key} = $value;

		return $this;
	}

	function __get($name) {
		if (property_exists($this, $name) || property_exists($this, "_" . $name)) throw new \Exception("Property exists, use ".get_class($this)."->get".Core::toCamel($name)."()");//die("Property exists, use ".get_class($this)."->get".Core::toCamel($name)."()");
		return array_key_exists($name, $this->_forced)
				? $this->_forced[$name]
				: ("Record::__get$name " . get_class($this));
	}

	function __set($name, $value) {
		return property_exists($this, $name) ? $this->{$name} = $value : $this->_forced[$name] = $value;
	}

	function __call($func, $arg) {
		if (substr($func, 0, 3) == "get") {
			if (array_key_exists(Core::fromCamel(substr($func, 3)), $this->_forced)) return $this->_forced[Core::fromCamel(substr($func, 3))];
		}
	}

	function __isset($name) {
		return TRUE;
	}

	function id() {
		return $this->id;
	}

	function getId() {
		return $this->id();
	}

	function __getMain() {
		$arr = array("Title", "Email", "Name", "Key", "Slug");

		foreach ($arr as $key) {
			if (method_exists($this, "get" . $key)) {
				return $this->{"get" . $key}();
			}
		}

		return null;
	}

	public function exists() {
		return is_int($this->id);
	}

	public function save() {
		return $this->id ? $this->update() : $this->insert();
	}

	public function toCleanArray($changedOnly = FALSE) {
		$arrData = $this->__toArray();

		unset($arrData["_forced"]);
		unset($arrData["_entity"]);
		unset($arrData["_changed"]);
		unset($arrData["_fields"]);

		if (array_key_exists("id", $arrData))
			unset($arrData["id"]);

		foreach ($arrData AS $key => $val)
			if (!in_array($key, $this->_fields))
				unset($arrData[$key]);

		if ($changedOnly)
			foreach ($arrData AS $key => $val)
				if (!in_array($key, $this->_changed))
					unset($arrData[$key]);

		return $arrData;
	}

	public function insert($entity = NULL) {
		if (is_null($entity))
			$entity = Context::getEntity($this->_entity);

		$arrData = $this->toCleanArray();

		foreach ($arrData AS $key => $val) {
			if (substr($key, 0, 1) == "_") {
				unset($arrData[$key]);
			}
		}

		if (empty($arrData))
			return NULL;

		$query = new Query\Insert();
		$query->setTable($entity->getTable())
				->setInsert($arrData);

		$prepare = DB::inst()->prepare($query->buildSQL());
		$insert = $prepare->execute();

		if ($insert)
			$this->id = DB::inst()->lastInsertId();

		return $insert ? $this : $insert;
	}

	public function update($entity = NULL) {
		if (is_null($entity))
			$entity = Context::getEntity($this->_entity);

		$arrData = $this->toCleanArray(TRUE);
		
		if (empty($arrData))
			return $this;

		$query = new \LFW\Query\Update();
		$query->setTable($entity->getTable())
				->setSet($arrData)
				->addWhere(" id = " . $this->getId());

		$prepare = DB::inst()->prepare($query->buildSQL());
		$update = $prepare->execute();

		return $update ? $this : $update;
	}

	public function delete($entity = NULL) {
		if (is_null($entity))
			$entity = Context::getEntity($this->_entity);

		$sql = 'DELETE FROM `' . $entity->getTable() . '` WHERE id = :id';

		$prepare = DB::inst()->prepare($sql);
		$delete = $prepare->execute(array("id" => $this->getId()));

		return $delete ? $this : $delete;
	}

	public function toArray($full = FALSE) {
		if ($full) return $this->__toArray();

		$reflectionClass = new \ReflectionClass($this);
		$arrProperties = $reflectionClass->getDefaultProperties();

		$arr = array();
		foreach ($arrProperties AS $property => $defaultValue)
			$arr[$property] = $this->{$property};

		return $arr;
	}

	public function __toArray() {
		$array = get_object_vars($this);

		foreach ($this->_forced AS $key => $val)
			if (!array_key_exists($key, $array))
				$array[$key] = $val;

		return $array;
	}

	public function getEntity($entity = NULL) {
		if (is_null($entity)) {
			$entity = str_replace("\Record\\", "\Entity\\", get_called_class()) . "s"; // @T00D00
		}

		if (class_exists($entity))
			return new $entity;

		$entity = implode("\\", array_slice(explode("\\", str_replace("\Record\\", "\Entity\\", get_called_class())), 0, -1)) . "\\" . $entity;

		if (class_exists($entity))
			return new $entity;

		throw new \Exception("Cannot get entity " . $entity);
	}

	public function getRecord($record = NULL) {
		if (class_exists($record))
			return new $record;

		if (is_null($record)) {
			$record = get_called_class();
		}

		$record = Generator::nameOne($record);

		if (class_exists($record))
			return new $record;

		$record = implode("\\", array_slice(explode("\\", get_called_class()), 0, -1)) . "\\" . $record;

		if (class_exists($record))
			return new $record;

		throw new \Exception("Cannot get record " . $record);
	}
}