<?php

namespace LFW;

class Entity {
	protected $table;
	protected $belongsTo, $hasMany, $hasOne;
	protected $collection;

	private $i18n = FALSE;
	
	protected $db = null;
	protected $query = null;
	protected $with = array();

	const ID = 'id';

	function __construct($db = null) {
		if (!$db) $this->db = Context::getDB();
		else $this->db = $db;

		$this->resetQuery();
	}

	function resetQuery() {
		$this->query = new Query\Select();
		$this->query->setSelect($this->table . ".*")
					->setTable($this->table)
					->setJoin(array())
					->setWhere(array())
					->setOrderBy(self::ID)
					->setLimit(100);

		$this->i18n = FALSE;

		return $this;
	}

	static function inst() {
		if (isset($this)) return $this;

		$class = get_called_class();
		return new $class;
	}

	function _getHasMany() {
		return $this->hasMany;
	}

	function whereArr($where, $link = "=") {
		foreach ($where AS $field => $value) {
			$this->where($field, $value, $link);
		}

		return $this;
	}

	function where($field, $value = NULL, $link = "=") {
		if (is_array($value)) {
			if ($link == "=")	$link = "IN";

			$value = "(".implode(",", $value).")";
		}

		if (!is_array($field))
			$field = array($field => $value);

		foreach ($field AS $key => $val) {
			$this->query->addWhere("`" . str_replace(".", "`.`", defined(get_class().'::.'.strtoupper($key))
					? str_replace(constant(get_class().'::.'.strtoupper($key)), ".", "`.`")
					: $key
				) . "` " . $link . " " . ($link != 'IN' ? $this->db->quote($val) : $val) . "");
		}

		return $this;
	}

	function orderBy($orderBy, $add = "ASC") {
		$this->query->setOrderBy($orderBy . " " . $add);
		return $this;
	}

	function setTable($table) {
		$this->table = $table;
		$this->query->setTable($table);
		return $this;
	}

	function getTable() {
		return $this->table;
	}

	function setCollection($collection) {
		$this->collection = $collection;
		return $this;
	}

	function getCollection() {
		return $this->collection;
	}

	function join($entity, $type = "INNER", $onJoin = null) {
		$first = null;
		$second = null;

		$i = $entity == "i18n";

		if (is_string($entity)) {
			if (isset($this->belongsTo[$entity])) {
				$foreignKey = $this->belongsTo[$entity]["foreign"];
				$entity = new $this->belongsTo[$entity]['entity'];
			} else if (isset($this->hasMany[$entity])) {
				$foreignKey = $this->hasMany[$entity]["foreign"];
				$entity = new $this->hasMany[$entity]['entity'];

				$first = $this->getTable() . ".id";
				$second = $entity->getTable() . "." . $foreignKey;
			} else {
				var_dump($this->hasMany);
				var_dump($this->belongsTo);
				die($entity);
			}
		} else {
			$entityHasMany = $entity->_getHasMany();

			if (isset($entityHasMany[$this->getTable()])) {
				$first = $entityHasMany[$this->getTable()]["table"] . "." . $entityHasMany[$this->getTable()]["foreign"];
				$second = $entity->getTable() . ".id";
			} else {
				$first = $this->getTable() . ".id";
				$second = $this->getTable() . ".id";
			}
		}

		$this->query->addJoin(strtoupper($type) . " JOIN " . $entity->getTable() . " ON (" . $first . " = " . $second . ($onJoin ? " AND " . $onJoin . " " : "") .") ");

		return $this;
	}

	function i18n() {
		$this->join('i18n', 'LEFT', $this->hasMany['i18n']["table"] . ".language_id = " . Lang::getCurrent());
		$this->query->prependSelect($this->hasMany['i18n']["table"] . ".*");

		$this->query->setOrderBy($this->table . ".id");
		$this->i18n = TRUE;

		return $this;
	}

	function getI18nForeign() {
		return $this->hasMany["i18n"]["foreign"];
	}

	function select($select) {
		$this->query->setSelect($select);
		return $this;
	}

	function prepareQuery() {
		return $this->db->prepare($this->query->buildSQL());
	}

	function findID($id) {
		$this->query->addWhere($this->table . "." . self::ID . " = " . $this->db->quote($id) . "");

		return $this->findOne();
	}

	function findAll() {
		$prepare = $this->prepareQuery();
		$query = $prepare->execute();

		$arr = $prepare->fetchAll();
		foreach ($arr AS &$row) {
			$row = $this->makeRecord($row);
		}

		$this->fillWith($arr);

		return new Record\Collection($arr);
	}

	function findAllKeyID() {
		return $this->findAll()->organizeByID();
	}

	function findList() {
		return $this->findAll()->getList();
	}

	function findListID() {
		return $this->findAll()->getListID();
	}

	function findOne() {
		$this->query->setLimit(1);

		$prepare = $this->prepareQuery();
		$query = $prepare->execute();
		$result = $prepare->fetch();

		if ($result) {
			return $this->makeRecord($result);
		}
		
		$this->resetQuery();

		return $result;
	}

	function makeRecord($result) {
		$r = new $this->collection;

		if ($this->i18n) {
			$r->forceSet($result);
		} else {
			$r->setArray($result);
		}

		return $r->emptyChanged();
	}

	function with($belongsTo) {
		$this->with[] = $belongsTo;
		return $this;
	}

	function fillWith($arr) {
		if (empty($arr)) return $arr;

		$transformed = false;
		if ($arr instanceof \LFW\Record) {
			$arr = array($arr);
			$transformed = true;
		}

		foreach ($this->with AS $with) {
			// check connection
			if (!isset($this->belongsTo[$with])) {
				throw new Exception\Runtime(get_class($this) . " is not connected to " . $with);
			}

			// find all ids
			$arrWithIDs = array();
			foreach ($arr AS $row) {
				$id = $row->get($this->belongsTo[$with]['current']);
				if ($id) {
					$arrWithIDs[$id] = true;
				}
			}

			// build query
			if (!empty($arrWithIDs)) {
				$entity = new $this->belongsTo[$with]['entity'];
				$arrData = $entity->where($this->belongsTo[$with]['foreign'], $arrWithIDs)->findAllKeyID();
			}

			// fill original data
			foreach ($arr AS $row) {
				$id = $row->get($this->belongsTo[$with]['current']);
				if ($id && isset($arrData[$id])) {
					$row->{'set' . \LFW\Core::toCamel($with)}($arrData[$id]);
				}
			}
		}

		return $transformed
			? $arr[0]
			: $arr;
	}

	public function getEntity($entity = NULL) {
		return Context::getEntity($entity);
	}

	public function getEmptyRecord() {
		return new $this->collection;
	}
}