<?php

namespace LFW;

class Generator {
  public static function nameOne($input) {
    $i18n = "";
    if (substr($input, -5) == "_i18n") {
      $i18n = "_i18n";
      $input = str_replace("_i18n", "", $input);
    } else if (substr($input, -4) == "i18n") {
      $i18n = "i18n";
      $input = str_replace("i18n", "", $input);
    }

    $id = substr($input, -3) == "_id";
    if ($id) $input = str_replace("_id", "", $input);

    $one = NULL;
    if (substr($input, -3) == "ies") //categories
      $one = substr($input, 0, -3) . "y";
    else if (substr($input, -3) == "ses") // statuses
      $one = substr($input, 0, -2);
    else if (substr($input, -3) == "es") // pages
      $one = substr($input, 0, -1);
    else if (substr($input, -1) != "s") // users_fb
      $one = $input;
    else  // users
      $one = substr($input, 0, -1);

    return $one . $i18n;
  }

  public static function nameMultiple($input) {
    $i18n = substr($input, -5) == "_i18n";
    $id = substr($input, -3) == "_id";

    if ($i18n) $input = str_replace("_i18n", "", $input);
    if ($id) $input = str_replace("_id", "", $input);

    $multiple = NULL;
    if (substr($input, -2) == "es") //categories
      $multiple = $input;
    else if (substr($input, -1) == "y") // category
      $multiple = substr($input, 0, -1) . "ies";
    else if (substr($input, -1) == "s") // category
      $multiple = $input;
    else
      $multiple = $input . "s";

    return $multiple . ($i18n ? "_i18n" : "");

  }

  public static function generateConfigFromDb($db) {
    $dbInst = DB::inst();
    $qTables = $dbInst->query("SHOW FULL TABLES FROM " . $db);
    $arrTables = array();
    while ($rTable = $qTables->fetchObject()) {
      $qFields = $dbInst->query("SHOW FULL COLUMNS FROM " . $rTable->{"Tables_in_".$db});
      $arrConf = array(
        "belongsTo" => array(),
        "hasMany" => array(),
        "fields" => array(),
      );
      while ($rField = $qFields->fetchObject()) {
        $field = array(
          "type" => $rField->Type,
          "null" => $rField->Null != "NO",
          "primary" => $rField->Key == "PRI",
          "default" => $rField->Default,
          "foreign" => substr($rField->Field, -3) == "_id",
        );
        $arrConf["fields"][$rField->Field] = $field;
      }
      $arrTables[$rTable->{"Tables_in_".$db}] = $arrConf;
    }

    foreach ($arrTables AS $table => $conf) {
      foreach ($conf["fields"] AS $name => $field) {
        if ($field["foreign"]) {
          $sqlForeign = "SELECT table_name, column_name, referenced_table_name, referenced_column_name
            FROM INFORMATION_SCHEMA.key_column_usage
            WHERE referenced_table_schema =  '" . $db . "' 
            AND table_name = '" . $table . "' 
            AND column_name = '" . $name . "' 
            AND referenced_table_name IS NOT NULL 
            ORDER BY table_name, column_name
            LIMIT 1";
          $qForeign = $dbInst->query($sqlForeign);
          $rForeign = $qForeign->fetchObject();

          if (!empty($rForeign)) { // if set in database
            $arrTables[$table]['belongsTo'][substr($name, 0, -3)] = array(
              "table" => $rForeign->referenced_table_name,
              "foreign" => $rForeign->referenced_column_name,
              "current" => $name,
              "field" => $name,
            );
          } else {
            foreach ($arrTables AS $tableChoice => $confChoice) {
              if (substr($tableChoice, -strlen(substr($name, -3)) - 2, strlen($name) - 3) == substr($name, 0, -3)) {
                //echo "Is table " . $tableChoice . " ok for " . $table . "." . $name . "?\n";
                $arrTables[$table]['belongsTo'][substr($name, 0, -3)] = array(
                  "table" => $tableChoice,
                  "foreign" => "id",
                  "current" => $name,
                  "field" => $name,
                );
              }
            }
            
            if (!isset($arrTables[$table]['belongsTo'][substr($name, 0, -3)])) {
              //echo "Trying to find the best choice for belongsTo $table.$name\n";
              foreach ($arrTables AS $tableChoice => $confChoice) {
                if (substr($tableChoice, -strlen(substr($name, -6)) - 2, strlen($name) - 3) == substr($name, 0, -3)) {
                  //echo "Is table " . $tableChoice . " ok?\n";
                  $arrTables[$table]['belongsTo'][substr($tableChoice, -strlen(substr($name, -6)) - 2, strlen($name) - 3)] = array(
                    "table" => $tableChoice,
                    "foreign" => "id",
                    "current" => $name,
                    "field" => $name,
                  );
                }
              }
            }

            if (!isset($arrTables[$table]['belongsTo'][substr($name, 0, -3)])) {
              //echo "Not set " . '$arrTables['.$table.'][\'belongsTo\'][substr('.$name.', 0, -3)]' . "\n";
              echo "Cannot find belongsTo " . $table.".".$name."\n";
            }
          }
        }
      }
    }

    foreach ($arrTables AS $table => $tableConf) { // logins
      foreach ($tableConf['belongsTo'] AS $name => $belongsTo) { // lfw_users
        if (isset($arrTables[$belongsTo['table']])) { // we have name in key of belongsTo
          $linkName = $table;
          if (substr($table, -4) == "i18n") {
            $linkName = "i18n";
          }

          // make duplicate and change linkname
          if (isset($arrTables[$belongsTo['table']]['hasMany'][$linkName])) {
            if (!isset($arrTables[$belongsTo['table']]['hasMany'][$linkName . '_' . $arrTables[$belongsTo['table']]['hasMany'][$linkName]['foreign']])) {
              $arrTables[$belongsTo['table']]['hasMany'][$linkName . '_' . $arrTables[$belongsTo['table']]['hasMany'][$linkName]['foreign']] = $arrTables[$belongsTo['table']]['hasMany'][$linkName];
            }

            $linkName = $linkName . '_' . $name . '_id';
          }

          $arrTables[$belongsTo['table']]['hasMany'][$linkName] = array(
            "foreign" => $name . "_id",
            "table" => $table,
            "field" => $name,
          );
        } else {
          echo "not set ". $belongsTo["table"];
          continue;
          echo "Trying to find the best choice for hasMany $table.$name _id\n";
          
          foreach ($arrTables AS $tableChoice => $confChoice) {
            if (substr($tableChoice, -strlen($name) - 1, strlen($name)) == $name) {
              echo "Is table " . $tableChoice . " ok?";
              /*$arrTables[$tableChoice]['hasMany'][$table] = array(
                "foreign" => $name . "_id",
                "table" => $table,
              );*/
            }
          }
        }
      }
    }

    $yaml = new \Symfony\Component\Yaml\Yaml;
    $notice = "# WARNING # This file is and should be auto generated.\n";
    file_put_contents("../config/generated/database.yml", $notice . $yaml->dump($arrTables));
    //var_dump($yaml->dump($arrTables));
    // var_dump($arrTables);
  }

  public function generateBaseFromConfig() {
    $yaml = new \Symfony\Component\Yaml\Yaml;
    $arrTables = $yaml->parse(file_get_contents("../config/generated/database.yml"));

    foreach ($arrTables AS $table => $conf) {
      $this->generateBaseEntity($table, $conf);
      $this->generateBaseRecord($table, $conf);
      //$this->generateBaseController($table);
      //$this->generateBaseLang($table);
    }
  }

  public static function generateBaseEntity($table, $conf) {
    $entity = $controller = Core::toCamel($table);
    $record = Core::toCamel(self::nameOne($table));
    $namespace = "Base";
    $collection = "\Base\Record\\" . Core::toCamel(self::nameOne($table));
    $i18n = substr($table, -4) == "i18n";

    $belongsTo = $conf['belongsTo'];
    $hasMany = $conf['hasMany'];

    $hasManyContent = "";
    $belongsToContent = "";
    $constantsContent = "";

    if (substr($entity, -4) == "I18n")
      $belongsTo["i18n"] = array(
        "entity" => "testmodel",
        "table" => substr($table, 0, -5),
        "foreign" => "id",
        "model" => "\Base\Records\\" . substr($entity, 0, -4),
      );


    foreach ($belongsTo as $name => $belongs) {
      if (empty($belongsToContent)) $belongsToContent = "\n";
      $belongsToContent .=
'   "' . $name . '" => array(
      "foreign" => "' . $belongs["foreign"] . '",
      "current" => "' . $belongs["current"] . '",
      "table" => "' . $belongs["table"] . '",
      "entity" => "\\' . $namespace . '\Entity\\' . Core::toCamel($belongs["table"]) . '",
    ),
';
    }

    foreach ($hasMany as $name => $has) {
      if (empty($hasManyContent)) $hasManyContent = "\n";
      $hasManyContent .=
'   "' . $name . '" => array(
      "foreign" => "' . $has["foreign"] . '",
      "table" => "' . $has["table"] . '",
      "entity" => "\\' . $namespace . '\Entity\\' . Core::toCamel($has["table"]) . '",
    ),
';
    }

    foreach ($conf['fields'] AS $field => $fieldConf) {
      $constantsContent .= "\tconst " . strtoupper($field)  . " = '".$table.".".$field."';" . "\n";
    }

    $hasManyContent .= empty($hasManyContent) ? "" : "\t";
    $belongsToContent .= empty($belongsToContent) ? "" : "\t";

    $entityBaseContent =
'<?php

/*
Autogenerated by LFW\Generator
All changes to this file will be lost at next autogenerate.
*/

namespace ' . $namespace . '\Entity;

class ' . $entity . ' extends \LFW\Entity {
  protected $table = "' . $table . '";

  protected $belongsTo = array(' . $belongsToContent . 
');
  protected $hasMany = array(' . $hasManyContent . 
');

  protected $collection = "' . $collection . '";
  
'.$constantsContent.'

  function __construct() {
    parent::__construct();
  }
}
';

    $entityContent =
'<?php

namespace ' . $namespace . '\Entity;

class ' . $entity . ' extends ' . $entity . 'Base {
  protected $collection = "' . $collection . '";

  function __construct() {
    parent::__construct();
  }
}
';

    $fpath = str_replace("\\", "/", $namespace) . "/Entity/" . Core::toCamel($entity) . ".php";

    if (!is_dir(SRC_PATH . dirname($fpath)))
      $ok = mkdir(SRC_PATH . dirname($fpath), 0755, TRUE);

    $ok = file_put_contents(SRC_PATH . $fpath, $entityBaseContent);

    /*if (!file_exists(SRC_PATH . $fullpath2)) {
      $ok2 = file_put_contents(SRC_PATH . $fullpath2, $entityContent);
    }*/
  }

  private static function findRecordByBelongsToField($belongsTo, $find) {
    foreach ($belongsTo AS $name => $conf) {
      if ($conf["field"] == $find)
        return $conf["table"];
    }

    return substr($find, 0, -3);
  }

  public static function generateBaseRecord($table, $conf) {
    $dbInst = DB::inst();
    $entity = $controller = Core::toCamel($table);
    $record = Core::toCamel(self::nameOne($table));
    $namespace = "Base";
    $collection = "\Base\Record\\" . Core::toCamel(self::nameOne($table));
    $i18n = substr($table, -4) == "i18n";
    $belongsTo = $conf['belongsTo'];

    try {
      $entity = "\\" . $namespace . "\Entity\\" . $entity;

      if (class_exists($entity))
        $objEntity = new $entity;

      else throw new \Exception("Cannot find entity");
    } catch (\Exception $e) {
      echo "Cannot find entity " . $entity . "\n";
      return;
    }

    $p = $dbInst->prepare("SHOW FULL COLUMNS FROM " . $table);
    $q = $p->execute();

    $arrFields = array();
    $fields = "";
    $joints = "";
    $setsGets = "";
    $constantsContent = "";
    while ($field = $p->fetch()) {
      $arrFields[] = $field->Field;
      $constantsContent .= "\tconst " . strtoupper($field->Field)  . " = '".$table.".".$field->Field."';" . "\n";

      $isBool = $field->Type == 'tinyint(1)';
      $isForeign = substr($field->Field, -3) == '_id';

      $fields .= 
' protected $' . $field->Field . (is_null($field->Default) ? ($field->Null == "YES" ? ' = NULL' : "" ) : ' = "' . $field->Default . '"') . ';' . "\n";
        
      $joints .= substr($field->Field, -3) == "_id"
        ? ' protected $_' . substr($field->Field, 0, -3) . ';' . "\n"
        : "";

      $setsGets .=
'

    public function set' . Core::toCamel($field->Field) . '($' . \lcfirst(Core::toCamel($field->Field)) .') {
      return $this->set(\'' . $field->Field . '\', ' . self::getCasting($field->Type) . '$' . \lcfirst(Core::toCamel($field->Field)) . ');
  }

  public function get' . Core::toCamel($field->Field) . '() {
    return ' . self::getCasting($field->Type) . '$this->' . $field->Field . ';
  }';

      if ($isForeign) {
        $setsGets .=
'

  public function set' . Core::toCamel(substr($field->Field, 0, -3)) . '($' . \lcfirst(Core::toCamel(substr($field->Field, 0, -3))) . ', $recursive = true) {
    $this->_' . substr($field->Field, 0, -3) . ' = $' . \lcfirst(Core::toCamel(substr($field->Field, 0, -3))) . ';
    if ($recursive) $this->set' . Core::toCamel($field->Field) . '($' . \lcfirst(Core::toCamel(substr($field->Field, 0, -3))) . ' ? $' . \lcfirst(Core::toCamel(substr($field->Field, 0, -3))) . '->getId() : null, false);
    return $this;
  }

  public function get' . Core::toCamel(substr($field->Field, 0, -3)) . '() {
    if (!isset($this->_' . substr($field->Field, 0, -3) . ')) {
      $tmp = new \Base\Entity\\' . Core::toCamel(self::findRecordByBelongsToField($belongsTo, $field->Field)) . '();
      $this->set' . Core::toCamel(substr($field->Field, 0, -3)) . '($tmp->findId($this->get' . Core::toCamel($field->Field) . '()));
    }
    return $this->_' . substr($field->Field, 0, -3) . ';
  }';
      }

      if ($isBool)
      $setsGets .=
'

  public function is' . Core::toCamel($field->Field) . '() {
    return (bool)$this->' . $field->Field . ';
  }';
    }

      $recordBaseContent =
'<?php

/*
Autogenerated by LFW\Generator
All changes to this file will be lost at next autogenerate.
*/

namespace ' . $namespace . '\Record;

class ' . $record . ' extends \LFW\Record {
  protected $_entity = "' . $entity . '";
  protected $_fields = array("' . implode('", "', $arrFields) . '");
' . $fields . '
' . $joints . '
  
'.$constantsContent.'

  function __construct() {
    parent::__construct();
  }'
. $setsGets . '   
}';

      $recordContent =
'<?php

namespace ' . $namespace . '\\' . $record . ';

class ' . $record . ' extends ' . $record . 'Base {
  protected $_entity = "\\' . $namespace . "\Entity\\" . $entity . '";
  
'.$constantsContent.'

  function __construct() {
    parent::__construct();
  }
}';


    $fullpath = str_replace("\\", "/", $namespace) . "/Record/" . Core::toCamel($record) . ".php";

    if (!is_dir(SRC_PATH . implode("/", array_slice(explode("/", $fullpath), 0, -1))))
      $ok = mkdir(SRC_PATH . implode("/", array_slice(explode("/", $fullpath), 0, -1)), 0755, TRUE);

    $ok = file_put_contents(SRC_PATH . $fullpath, $recordBaseContent);
  }

  public static function generateBaseController($table) {
    $entity = $controller = Core::toCamel($table);
    $record = Core::toCamel(self::nameOne($table));
    $namespace = "Base";
    $collection = "\Base\Record\\" . Core::toCamel(self::nameOne($table));
    $i18n = substr($table, -4) == "i18n";

    $controllerBaseContent =
'<?php

/*
Autogenerated by LFW\Generator
All changes to this file will be lost at next autogenerate.
*/

namespace ' . $namespace . '\Controller;

class ' . $controller . ' extends \LFW\Controller {
/*  function __construct() {
    parent::__construct();
  }

  function oneAction($_router, $_view) {
    $this->one = $this->' . \lcfirst($record) . ' = $this->getEntity()->findID($_router->get("id"));
    
    return new $_view("one");
  }

  function allAction($_maestro) {
    $this->all = $this->' . \lcfirst($entity) . ' = $this->getEntity()->findAll();

    return $_maestro->all($this->all, array(
      "title" => __("' . \lcfirst($entity) . '"),
      "heading" => array(
        "id" => "#",
        "title" => __("title"),
      ),
    ));
  }

  function addAction($_maestro, $_view){
    $this->one = $this->' . \lcfirst($record) . ' = $this->getRecord();

    return $_maestro->add(array(
      "title" => __("' . \lcfirst($record) . '"),
      "content" => new $_view("maestro/' . \lcfirst($entity) . '/add"),
    ));
  }

  function insertAction($_maestro, $_request) {
    $this->one = $this->getRecord()->set($_request->getPost()->' . \lcfirst($record) . ');

    return $_maestro->insert($this->one, "' . \lcfirst($entity) . '", TRUE);
  }

  function editAction($_maestro, $_router, $_view) {
    $this->one = $this->' . \lcfirst($record) . ' = $this->getEntity()->findID($_router->get("id"));

    return $_maestro->edit(array(), array(
      "title" => __("' . \lcfirst($record) . '"),
      "content" => new $_view("maestro/' . \lcfirst($entity) . '/edit"),
    ));
  }

  function updateAction($_maestro, $_request) {
    return $_maestro->update($this->getRecord()->set($_request->getPost()->' . \lcfirst($record) . '), "' . \lcfirst($entity) . '", TRUE);
  }

  function deleteAction($_maestro, $_router) {
    return $_maestro->delete($this->getEntity()->findID($_router->get("id")), "' . \lcfirst($entity) . '", FALSE);
  }*/
}';

      $controllerContent =
'<?php

namespace ' . $namespace . '\Controller;

class ' . $controller . ' extends ' . $controller . 'Base {
  function __construct() {
    parent::__construct();
  }
}';

    $fullpath = str_replace("\\", "/", $namespace) . "/Controller/" . Core::toCamel($controller) . ".php";

    if (!is_dir(SRC_PATH . implode("/", array_slice(explode("/", $fullpath), 0, -1))))
      $ok = mkdir(SRC_PATH . implode("/", array_slice(explode("/", $fullpath), 0, -1)), 0755, TRUE);

    $ok = file_put_contents(SRC_PATH . $fullpath, $controllerBaseContent);
  }

  public static function generateBaseLang($table) {
    return;
    $controllerBaseContent =
'<?php

/*
Autogenerated by LFW\Generator
All changes to this file will be lost at next autogenerate.
*/

namespace ' . $namespace . '\Controller;

class ' . $controller . ' extends \LFW\Controller {
  function __construct() {
    parent::__construct();
  }

  function oneAction($_router, $_view) {
    return new $_view("one", array("' . \lcfirst($record) . '" => $this->getEntity()->findID($_router->get("id"))));
  }

  function allAction($_maestro) {
    return $_maestro->all($this->getEntity()->findAll(), array(
      "title" => __("' . \lcfirst($entity) . '"),
      "heading" => array(
        "id" => "#",
        "title" => __("title"),
      ),
    ));
  }

  function addAction($_maestro, $_view){
    $' . \lcfirst($record) . ' = $this->getRecord();

    return $_maestro->add(array(
      "title" => __("' . \lcfirst($record) . '"),
      "content" => new $_view("maestro/' . \lcfirst($entity) . '/add", array(
        "' . \lcfirst($record) . '" => $' . \lcfirst($record) . ',
      )),
    ));
  }

  function insertAction($_maestro, $_request) {
    return $_maestro->insert($this->getRecord()->set($_request->getPost()->' . \lcfirst($record) . '), "' . \lcfirst($entity) . '", TRUE);
  }

  function editAction($_maestro, $_router, $_view) {
    $' . \lcfirst($record) . ' = $this->getEntity()->findID($_router->get("id"));

    return $_maestro->edit(array(), array(
      "title" => __("' . \lcfirst($record) . '"),
      "content" => new $_view("maestro/' . \lcfirst($entity) . '/edit", array(
        "page" => $' . \lcfirst($record) . ',
      )),
    ));
  }

  function updateAction($_maestro, $_request) {
    return $_maestro->update($this->getRecord()->set($_request->getPost()->' . \lcfirst($record) . '), "' . \lcfirst($entity) . '", TRUE);
  }

  function deleteAction($_maestro, $_router) {
    return $_maestro->delete($this->getEntity()->findID($_router->get("id")), "' . \lcfirst($entity) . '", FALSE);
  }
}';

      $controllerContent =
'<?php

namespace ' . $namespace . '\Controller;

class ' . $controller . ' extends ' . $controller . 'Base {
  function __construct() {
    parent::__construct();
  }
}';

    $fullpath = str_replace("\\", "/", $namespace) . "/Controller/" . Core::toCamel($controller) . ".php";

    if (!is_dir(SRC_PATH . implode("/", array_slice(explode("/", $fullpath), 0, -1))))
      $ok = mkdir(SRC_PATH . implode("/", array_slice(explode("/", $fullpath), 0, -1)), 0755, TRUE);

    $ok = file_put_contents(SRC_PATH . $fullpath, $controllerBaseContent);
  }

  private static function getCasting($type) {
    return strpos($type, "varchar") === 0 || strpos($type, "text") === 0 
        ? "(string)" 
        : (strpos($type, "int") === 0
            ? "(int)"
            : (strpos($type, "float") === 0 || strpos($type, "float") === 0
              ? "(float)"
              : (strpos($type, "bool") === 0
                ? "(bool)"
                : ($type == 'tinyint(1)'
                  ? "(bool)"
                  : "")
              )
            )
          );
  }
}

?>