<?php

namespace LFW;

class DI2 {
	private $class;
	function __construct($class){
		$this->class = $class;
	}

	public function __call($method, $args){
		if (is_string($this->class))
			$this->class = new $this->class;

        if(is_callable(array($this->class, $method))) {
            return call_user_func_array(array($this->class, $method), $args);
        }

        throw new \Exception("Cannot di2 call " .$method);
    }
}

class DI {
	public static $container;
	public static $links;

    public static function dump() {
    	//var_dump(self::$container);
    }

    private static function lazy($c, $aka = NULL) {

    }

	public static function di($input, $aka = NULL) {
		$class = is_string($input) ? $input : get_class($input);
		$obj = is_object($input) ? $input : NULL;

		if ($class[0] == "\\")
			$class = substr($class, 1);

		if (isset(DI::$container[$class]))
			return DI::$container[$class];

		if (isset(DI::$container[$aka]))
			return DI::$container[$aka];

		if (!isset(DI::$container[$class]) && is_object($obj)) {
			DI::$container[$class] = $obj;

			if (!is_null($aka))
				DI::$container[$aka] = DI::$container[$class];
		}

		if (!is_object($obj)) {
			DI::$container[$class] = new DI2($class);

			if (!is_null($aka))
				DI::$container[$aka] = DI::$container[$class];
		}

		return DI::$container[$class];
	}
}

?>