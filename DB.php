<?php

namespace LFW;

class DB {
  protected static $db = array();

  static function push($db) {
    $index = count(self::$db);

    self::$db[$index] = $db;

    return $index;
  }

  static function inst($index = 0) {
    return self::$inst[$index];
  }
}