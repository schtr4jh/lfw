<?php

namespace LFW\Query;

class Delete extends \LFW\Query {
	protected $insert;

	// builders
	function buildSQL() {
		return "DELETE FROM `" . $this->table . "` " .
			($this->where ? $this->buildWhere() : '') .
			($this->limit ? 'LIMIT ' $this->limit : '');
	}
}