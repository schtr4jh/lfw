<?php

namespace LFW\Query;

use \LFW\Context;

class Update extends \LFW\Query {
	protected $set;

	// builders
	function buildSQL() {
		return "UPDATE `" . $this->table . "` " .
			"SET " . $this->buildSet() . " " .
			($this->where ? $this->buildWhere() : '') .
			($this->limit ? ' LIMIT ' . $this->limit : '');
	}

	// builders

	function buildSet() {
		$arrValues = array();

		foreach ($this->set AS $key => $val) {
			$arrKey = "`" . $key . "` = ";

			$arrVal = "";
			if (is_bool($val)) {
				$arrVal .= $val ? 1 : 0;
			} else if (empty($val)) {
				$arrVal .= "NULL";
			} else {
				$arrVal .= Context::getDB()->quote($val);
			}

			$arrValues[] = $arrKey . $arrVal;
		}

		return implode(", ", $arrValues);
	}

	// setters
	function setSet($set) {
		$this->set = $set;
		return $this;
	}

	function buildWhere() {
		return $this->where ? ' WHERE ' . implode(" AND ", $this->where) : '';
	}

	function setTable($table) {
		$this->table = $table;
		return $this;
	}

	function setWhere($where) {
		$this->where = $where;
		return $this;
	}

	function setLimit($limit) {
		$this->limit = $limit;
		return $this;
	}

	// adders
	function addWhere($where) {
		$this->where[] = $where;
		return $this;
	}
}