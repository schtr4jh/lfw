<?php

namespace LFW\Query;

class Select extends \LFW\Query {
	protected $select;

	// builders
	function buildSQL() {
		$sql = "SELECT " . $this->select . " " .
			"FROM `" . $this->table . "` " .
			($this->join ? $this->buildJoin() : '') .
			($this->where ? $this->buildWhere() : '') .
			($this->having ? $this->buildHaving() : '') .
			($this->groupBy ? ' GROUP BY ' . $this->groupBy : '') .
			($this->orderBy ? ' ORDER BY ' . ($this->orderBy == 'id' ? $this->table . "." . $this->orderBy: $this->orderBy) : '') .
			($this->limit ? ' LIMIT ' . $this->limit : '') . "\n";
		return $sql;
	}

	function setSelect($select) {
		$this->select = $select;
		return $this;
	}

	function addSelect($select) {
		if ($this->select) {
			$this->select .= ', ';
		}

		$this->select .= $select;
		return $this;
	}

	function prependSelect($select) {
		if ($this->select) {
			$select .= ', ';
		}

		$this->select = $select . $this->select;
		return $this;
	}
}