<?php

namespace LFW\Query;

use \LFW\Context;

class Insert extends \LFW\Query {
	protected $insert;

	// builders
	function buildSQL() {
		return "INSERT INTO `" . $this->table . "` " .
			 $this->buildKeys() . 
			"VALUES " . $this->buildValues();
	}
	
	function setInsert($insert) {
		$this->insert = $insert;
		return $insert;
	}

	function buildKeys() {
		$arrKeys = array();

		foreach ($this->insert AS $key => $val) {
			$arrKeys[] = "`" . $key . "`";
		}

		return "(" . implode(", ", $arrKeys) . ") ";
	}

	function buildValues() {
		$arrValues = array();

		foreach ($this->insert AS $key => $val) {
			$arrVal = "";
			if (is_bool($val)) {
				$arrVal .= $val ? 1 : 0;
			} else if (empty($val)) {
				$arrVal .= "NULL";
			} else {
				$arrVal .= Context::getDB()->quote($val);
			}

			$arrValues[] = $arrVal;
		}

		return "(" . implode(", ", $arrValues) . ") ";
	}
}