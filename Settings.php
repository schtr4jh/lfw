<?php

namespace LFW;

class Settings {
   	private static $settings = array();
    
    public static function set($key, $val) {
        self::$settings[$key] = $val;
    }
    public static function get($key) {
        return isset(self::$settings[$key]) ? self::$settings[$key] : FALSE;
    }
}

?>