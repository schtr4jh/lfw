<?php

namespace LFW\Cache;

use \LFW\Cache;

class APC extends Cache {
  private static $cache;
  private static $instances;
  protected $apc;

  function __construct() {
    parent::__construct($this);
  }

  function get($key, $default = null) {
    $value = apc_fetch($key, $success);

    return $success
      ? $value
      : (is_callable($default)
        ? $default()
        : $default
      );
  }

  function set($key, $value, $time = "1 hour") {
    apc_store($key, $value, strtotime("+".$time)-mktime());
    return $this;
  }

  function exists($key) {
    return apc_exists($key);
  }

  function delete($key) {
    return apc_delete($key);
  }
}

?>