<?php

namespace LFW;

class Controller {
	use \LFW\Traits\Get;

	public $entity = null;
	public $record = null;
	public $form = null;

	public function __construct() {
		$class = get_called_class();

		if (strpos($class, '\Controller\\')) {
			$lang = SRC_PATH . implode("/", explode("\\", strtolower(substr($class, 0, strpos($class, '\Controller\\'))))) . "/langs/lang.php";

			if (is_file($lang)) {
				include_once $lang;
			}
		}
	}

	public function __toArray() {
		return (array)$this;
	}

	function getContext() {
		return Core::getContext();
	}
}

?>