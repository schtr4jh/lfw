<?php

namespace LFW;

class Lang {
    const EN = 1;
    const SI = 2;
    // const fr_CA = 3;
    // const fr_FR = 4;
    // const FR = 4;

	protected static $languages = array(
		self::EN => array(
            "title" => "English",
            "code" => "en",
            "international" => "en_EN",
        ),
		self::SI =>  array(
            "title" => "Slovenski",
            "code" => "slo",
            "international" => "sl_SI"
        ),
	);

	protected static $default = self::EN;
	protected static $current = self::EN;

	protected static $values;
    
    public static function set($lang, $key, $val) {
        self::$values[$lang][$key] = $val;
    }

    public static function setArray($lang, $arr) {
        foreach ($arr AS $key => $val) {
            self::set($lang, $key, $val);
        }
    }

    public static function get($key, $lang = NULL) {
    	$lang = is_null($lang) ? self::$current : $lang;

        return isset(self::$values[$lang][$key])
        	? self::$values[$lang][$key]
        	: (isset(self::$values[self::$default][$key])
        		? self::$values[self::$default][$key]
        		: $key);
    }

    public static function lang($lang = NULL) {
        if (is_null($lang))
            return self::$current;
        
        self::$current = $lang;
    }

    /*
    @param int|string $locale
    */
    public static function setLocale($locale, $default = FALSE) {
        $validate = Context::get('Validate');

        if ($validate->isInt($locale) && array_key_exists($locale, self::$languages)) {
            $default ? self::$current = $locale : self::$default = $locale;
        } else throw new Exception("Cannot set locale.");
    }

    public static function getCurrent(){
        return self::$current;
    }
}

?>