<?php

namespace LFW;

class Assets {
	private static $assets = array();

	static function add($assets, $path = NULL) {
		if (empty($path)) {
			$backtrace = debug_backtrace();
			$path = str_replace(array("\\", "controller"), array("/", "public"), strtolower($backtrace[1]["class"]));
		}

		if (!is_array($assets))
			$assets = (array)$assets;

		foreach ($assets AS $asset)
			if (!in_array($path . DS . $asset, self::$assets))
				$assets[] = $path . DS . $asset;
	}

	static function get() {
		return self::$assets;
	}
}

?>