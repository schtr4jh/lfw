<?php

namespace LFW\Helpers;

use LFW\Settings;

class Optimize {
  private static $files = array();
  public static $types = array();
  private static $content = array();
  
  /*
   * Konfiguracija tipov datotek.
   */
  public static function setTypes() {
    self::$types = array(
      "css" => array(
        "header" => "text/css",
        "ext" => "css",
        "html" => '<link rel="stylesheet" type="text/css" href="##LINK##" />'
      ),
      /*"less" => array(
        "header" => "text/css",
        "ext" => "css",
        "html" => '<link rel="stylesheet/less" type="text/css" href="##LINK##" />'
      ),*/
      "js" => array(
        "header" => "text/javascript",
        "ext" => "js",
        "html" => '<script type="text/javascript" src="##LINK##"></script>'
      )
    );
  }

  public static function addAssets($files) {
    $backtrace = debug_backtrace();

    if (!is_array($files))
      $files = (array)$files;
      
    foreach ($files AS $file) {
      self::addFile(substr(strrchr($file,'.'),1), "src/" . implode("/", array_slice(explode("/", str_replace("\\", "/", $backtrace[1]["class"])), 0, -2)) . "/public/" . $file, $backtrace[1]["class"]);
    }
  }
  
  /*
   * Metoda za dodajanje datotek.
   * 
   * $type - eden izmed zgoraj navedenih tipov datotek (prvi index)
   * $files - ena (string)  ali več datotek (enodimenzionalen array)
   * $section - privzeto main, lahko se doda dodatna sekcija. Za vsako sekcijo se ustvari svoja cache datoteka (main za datoteke, ki se vedno postavijo na strani in poljubno število drugih za v večini nepomembne datoteke)
   */
  public static function addFile($type, $files, $section = "main") {
    // type must be set
    if (!isset(self::$types[$type]) || empty($files))
      return FALSE;
    
    // if files are not in array, we make array
    if (!is_array($files))
      $files = array($files);
      
    foreach ($files AS $file)
      if (!isset(self::$files[$type][$section]) || !in_array($file, self::$files[$type][$section]))
        self::$files[$type][$section][] = $file;
  }
  
  /*
   * Generira končni HTML.
   */
  public static function getHTML($types = NULL) {
    $html = array();
    $types = is_null($types)
      ? array("css", "js")
      : is_array($types)
        ? $types
        : array($types);
    foreach (self::$types AS $type => $conf) {
      if (!is_null($types) && !in_array($type, $types)) {
        continue;
      }
      $dir = "cache/" . $type . "/";
        
      // create cache dir
      if (!is_dir(WWW_PATH . $dir)) {
        if (is_writable(WWW_PATH . $dir))
          mkdir(WWW_PATH . $dir, 777, TRUE);
        else
          throw new \Exception("Cannot write to " . WWW_PATH . $dir);
      }
        
      // foreach filetype as section
      if (isset(self::$files[$type]))
      foreach (self::$files[$type] AS $section) {
        $lastchange = self::getChangeTime($section);

        $hash = sha1(implode($section) . $lastchange);

        $filename = $hash . "." . self::$types[$type]['ext'];
        
        if (TRUE || Settings::get("dev")) {
          foreach ($section AS $key => $file) {
            $html[] = str_replace("##LINK##", "/" . $file, self::$types[$type]['html']);
          }
        } else {
          // if file doesn't exist, we create it
          if (!is_file(WWW_PATH . $dir . $filename) || self::isOldCache($section, $filename)) {
            $fileContent = NULL;

            // foreach section as file
            foreach ($section AS $key => $file)
              if (end(explode(".", $file)) == $conf['ext']
                && ((is_file(WWW_PATH . $file) && $content = file_get_contents(WWW_PATH . $file)) || (is_file(APP_PATH . $file) && $content = file_get_contents(APP_PATH . $file)))) {
                $fileContent .= $content;
              }
            
            // save file
            if (is_writable(WWW_PATH . $dir))
              file_put_contents(WWW_PATH . $dir . $filename, ($type !== "css" ? $fileContent : self::compress($fileContent)));
            else
              Debug::add("w", "Can't save cache file '" . WWW_PATH . $dir . $filename ."'. Check your CHMOD.");
          }
          
          $html[] = str_replace("##LINK##", "/" . $dir . $filename, self::$types[$type]['html']);
        }
      }
    }
    
    return '<!-- optimize -->' . implode("\n", $html) . '<!-- /optimize -->';
  }

  private static function compress($buffer) {
        /* remove comments */
        $buffer = preg_replace("/((?:\/\*(?:[^*]|(?:\*+[^*\/]))*\*+\/)|(?:\/\/.*))/", "", $buffer);
        /* remove tabs, spaces, newlines, etc. */
        $buffer = str_replace(array("\r\n","\r","\t","\n",'  ','    ','     '), '', $buffer);
        /* remove other spaces before/after ) */
        $buffer = preg_replace(array('(( )+\))','(\)( )+)'), ')', $buffer);
        return $buffer;
    }

    public static function getChangeTime($section) {
      $time = 0;

    if (Settings::get("dev"))
      return $time;

    foreach ($section AS $key => $file) {
      if (is_file(WWW_PATH . $file)) {
        if (filemtime(WWW_PATH . $file) > $time) {
          $time = filemtime(WWW_PATH . $file);
        }
      }
    }
    
    return $time;
    }
  
  /*
   * Preveri starost cachea
   * 
   * $section - ime sekcije
   * $filename - ime datoteke
   */
  public static function isOldCache($section, $filename) {
    return is_file($filename) ? self::getChangeTime($section) > filemtime($filename) : true;
  }
}

Optimize::setTypes();

?>