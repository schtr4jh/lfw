<?php

namespace LFW\Helpers;

use LFW\Settings;
use LFW\Context;

class Auth {
	public static $users;
	public $statuses;
	private static $user;
	
	public static function addFlag($flags = array()) {
		if (!is_array($flags))
			$flags = (array)$flags;

		foreach ($flags AS $flag) {
			if (!in_array($flag, $_SESSION['Auth']['flags']))
				$_SESSION['Auth']['flags'][] = $flag;
		}
	}

	public static function hasFlag($flags = array()) {
		if (!isset($_SESSION['Auth']['flags']))
			return FALSE;

		if (!is_array($flags))
			$flags = (array)$flags;

		foreach ($flags AS $flag) {
			if (!in_array($flag, $_SESSION['Auth']['flags']))
				return FALSE;
		}

		return TRUE;
	}

	public static function removeFlag($flags = array()) {
		foreach ($flags AS $flag) {
			if (($key = array_search($flag, $_SESSION['Auth']['flags'])) !== false) {
			    unset($_SESSION['Auth']['flags'][$key]);
			}
		}
	}

	private static function setUsersEntity() {
		$config = Settings::get("_lfw_config");
		$authConfig = $config["defaults"]["auth"];
		return self::$users = new $authConfig["user"]["entity"];
	}

	private static function setUserRecord() {
		$config = Settings::get("_lfw_config");
		$authConfig = $config["defaults"]["auth"];
		return self::$user = new $authConfig["user"]["record"];
	}

	// user object
	public static function getUser() {
		if (self::$user) return self::$user;

		if (!self::$users) self::setUsersEntity();

		return self::$user = self::$users->findID(isset($_SESSION['Auth']['user_id']) ?: -1) ?: self::setUserRecord();
	}

	public static function user($key = null) {
		return isset($_SESSION['User'])
			? ($key
				? (isset($_SESSION['User'][$key]) ?: NULL)
				: $_SESSION['User']
				)
			: NULL;
	}

	public static function makePassword($password, $hash = NULL) {
		$hash = is_null($hash) ? Settings::get("hash") : $hash;

		return sha1($password . $hash);
	}

	public static function login($email, $password, $hash = NULL) {
		$hash = is_null($hash) ? Settings::get("hash") : $hash;

		$config = Settings::get("_lfw_config");

		if (!isset($config['defaults']['auth']['user']['entity']))
			throw new \Exception("Auth user entity not set (config).");

		self::$users = new $config['defaults']['auth']['user']['entity'];
		
		$rUser = self::$users->where("email", $email)->where("password", self::makePassword($password, $hash))->findOne();
		
		if ($rUser) {
			return self::performLogin($rUser);
		}

		return FALSE;
	}

	public static function loginByUserID($id) {
		$sql = "SELECT u.* 
			FROM users u
			WHERE u.id = '" . Context::getDB()->escape($id) . "'";
		$q = Context::getDB()->query($sql);
		$r = Context::getDB()->fetch($q);

		if ($r) {
			return self::performLogin($r);
		}

		return FALSE;
	}

	public static function loginByAutologin($autologin) {
		$sql = "SELECT u.* 
			FROM users u
			INNER JOIN lfw_users_autologin ua ON (ua.user_id = u.id AND ua.active = 1)
			WHERE ua.autologin = '" . Context::getDB()->escape($autologin) . "'";
		$q = Context::getDB()->query($sql);
		$r = Context::getDB()->fetch($q);

		if ($r) {
			return self::performLogin($r);
		}

		return FALSE;
	}

	public static function hashLogin($hash) {
		$sql = "SELECT u.* 
			FROM lfw_users u
			INNER JOIN lfw_logins l ON (l.user_id = u.id)
			WHERE l.hash = '" . Context::getDB()->escape($hash) . "' 
			AND l.dt_out = '0000-00-00 00:00:00'
			AND l.ip = '" . $_SERVER['REMOTE_ADDR'] . "'";
		$q = Context::getDB()->query($sql);
		$r = Context::getDB()->fetch($q);

		if ($r) {
			return self::performLogin($r);
		}

		return FALSE;
	}

	private static function performLogin($rUser) {
		$sessionHash = sha1(microtime() . sha1($rUser->getId()) . Settings::get("hash"));
		$dtIn = date("Y-m-d H:i:s");

		$sql = "INSERT INTO logins (user_id, dt_in, dt_out, hash, ip) " .
			"VALUES (:user_id, :dt_in, '0000-00-00 00:00:00', :hash, :ip)";

		$db = Context::getDB();
		$prepare = $db->prepare($sql);

		$q = $prepare->execute(array("user_id" => $rUser->getId(), "dt_in" => $dtIn, "hash" => $sessionHash, "ip" => $_SERVER['REMOTE_ADDR']));

		if ($q) {
			$_SESSION['User'] = $rUser->toArray();

			$_SESSION['Auth'] = array(
				"user_id" => $rUser->getId(),
				"dt_in" => $dtIn,
				"dt_out" => '0000-00-00 00:00:00',
				"hash" => $sessionHash,
				"ip" => $_SERVER['REMOTE_ADDR'],
				"flags" => array(),
			);

			$config = Settings::get("_lfw_config");

			setcookie("LFW", serialize(array("hash" => $sessionHash)), time() + (24*60*60*365.25), "/", $config['defaults']['domain']);
			
			return TRUE;
		}

		return FALSE;
	}

	public static function logout() {
		if (isset($_SESSION['Auth']))
			Context::getDB()->query("UPDATE lfw_logins SET dt_out = '" . date("Y-m-d H:i:s") . "' WHERE hash = '" . Context::getDB()->escape($_SESSION['Auth']['hash']) . "'");
		
		unset($_SESSION['User']);
		unset($_SESSION['Auth']);
		unset($_COOKIE['LFW']);
	}

	public static function isLoggedIn($try = false) {
		// valid session login
		$sessionLogin = !empty($_SESSION['Auth']['user_id']) 
			&& !empty($_SESSION['User']['id']) 
			&& !empty($_COOKIE['LFW']) 
			&& ($cookie = unserialize($_COOKIE['LFW']))
			&& $cookie['hash'] == $_SESSION['Auth']['hash'];

		if ($sessionLogin) return true;

		if (!$try) return false;

		// cookie login
		$cookieLogin = (isset($_COOKIE['LFW']) && ($cookie = unserialize($_COOKIE['LFW'])) && isset($cookie['hash']))
			? self::loginByAutologin($cookie['hash'])
			: false;

		if ($cookieLogin) return true;

		return false;
	}
}

?>