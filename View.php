<?php

namespace LFW;

class View {
	protected $view;
	protected $data;
	protected static $globals = array();

	function __construct($view = NULL, $data = array()) {
		if (is_string($view))
			$this->view = new \LFW\View\Twig($view, $data);
	}

	function addGlobal($key, $val) {
		self::$globals[$key] = $val;
	}

	function setData($arrData) {
		$this->data = $arrData;
	}

	function addData($arrData = array()) {
		foreach ($arrData AS $key => $data) {
			if (!isset($this->data[$key]))
				$this->data[$key] = $data;
			else if (is_array($this->data[$key]))
				$this->data[$key][] = $data;
			else
				$this->data[$key] = $data;
		}
	}

	function getData() {
		return $this->data;
	}

	function autoparse() {
		if (!Settings::get("dev") && ob_get_length())
			ob_end_clean();
		
		return $this->view->autoparse();
	}

	function __toString() {
		return is_string($this->view) || is_string($this->view) ? $this->view : $this->view->__toString();
	}
}

?>