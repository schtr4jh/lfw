<?php

namespace LFW {

class Core {
	public static $layout, $controller, $view;
	private $env = "pro";
	private $app = null;
	public static $core;
	public static $context = null;

	function __construct($url = NULL, $environment = "pro") {
		self::$core = &$this;
		try {
			$this->define();
			$this->nullByteProtection();
			$this->initEnvironment($environment);
			$this->setContext();
			$this->parseDefaults();
			$this->setApp();
			$this->setDefaults();
			$this->initSession();
			$this->initDatabase();
			$this->initCache();
			$this->buildRouter();
			$this->initI18n();
			$this->runURL();
		} catch (\Exception $e) {
			$this->handleException($e, $url);
		}
	}

	private function handleException($e, $currentUrl = null) {
		if ($this->env == 'dev') {
		    $whoops = new \Whoops\Run;
			$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
			$whoops->register();
			$whoops->handleException($e);
		} else {
			Debug::addError($e->getMessage());

			if ($this->env == 'dev') {
				Debug::addError("Check: " . $e->getFile() . ":" . $e->getLine());
			}

			$this->logException($e);
			
			if (!$currentUrl || $currentUrl != "/") {
				Status::internal("/");
			}
		}
	}

	private function logException($e) {
		$nl = "\n";
		$sep = str_repeat("=", 100);
		$content = DT_NOW . "     " . $_SERVER['REQUEST_URI'] . "     " . $_SERVER['REMOTE_ADDR'] . $nl . $sep . $nl .
			$e->getMessage() . $nl .
			$e->getFile() . ":" . $e->getLine() . " - " . $nl . 
			$e->getTraceAsString() . $nl . $sep . $nl .
			'Exception: ' . $nl . print_r($e, TRUE) . $nl . $sep . $nl .
			'Debug backtrace: ' . $nl . print_r(debug_backtrace(), TRUE) . $nl . $sep . $nl .
			'$_SERVER: ' . $nl . print_r($_SERVER, TRUE) . $nl . $sep . $nl .
			(!empty($_POST) ? '$_POST: ' . $nl . print_r($_POST, TRUE) . $nl . $sep . $nl : "") . 
			(!empty($_GET) ? '$_GET: ' . $nl . print_r($_GET, TRUE) . $nl . $sep . $nl : "") . 
			(!empty($_SESSION) ? '$_SESSION: ' . $nl . print_r($_SESSION, TRUE) . $nl . $sep . $nl : "") .
			(!empty($_COOKIE) ? '$_COOKIE: ' . $nl . print_r($_COOKIE, TRUE) . $nl . $sep . $nl : "");

		file_put_contents(
			LOG_PATH . date("Y-m-d-H-i-s") . "_" . substr(sha1($content), 0, 8) . ".log",
			$content
		);

		if ($this->env == 'dev') {
			echo "<pre>" . $content . "</pre>";
		}
	}

	private function setContext() {
		try {
			self::$context = new Context();
		} catch (Exception $e) {
			throw new Exception\Runtime("Cannot set context.");
		}
	}

	private function define() {
		// directories
		define("DS", DIRECTORY_SEPARATOR);
		define("ROOT_PATH", $_SERVER['DOCUMENT_ROOT']);
		define("LIB_PATH", ROOT_PATH . "lib" . DS);
		define("LOG_PATH", ROOT_PATH . "logs" . DS);
		define("SRC_PATH", ROOT_PATH . "src" . DS);
		define("WWW_PATH", ROOT_PATH . "www" . DS);
		define("CORE_PATH", SRC_PATH . "core" . DS);
		define("CACHE_PATH", WWW_PATH . "cache" . DS);
		define("TMP_PATH", CACHE_PATH . "tmp" . DS);
		define("UPLOADS_PATH", WWW_PATH . "uploads" . DS);
		
		// "helpers"
		define("DT_NOW", date("Y-m-d H:i:s"));
		define("DT_NULL", "0000-00-00 00:00:00");
	}

	private function nullByteProtection() {
		try {
			foreach (array('_GET', '_POST', '_SERVER') AS $v) {
				if (isset($$v)) {
					array_walk_recursive($$v, function($val, $key){
						if(strpos($val, chr(0)) !== false) {
							throw new Exception\Security("Null byte attack.");
						}
					});
				}
			}
		} catch (Exception\Security $e) {
			throw new Exception\Security("Hacking attempt", 0, $e);
		}
	}

	private function parseDefaults($dir = ROOT_PATH) {
		try {
			if ($this->env == "pro" && is_file(CACHE_PATH . "lfw" . DS . "pro_lfw_config_" . sha1($dir) . ".php")) {
				Settings::set("_lfw_config", json_decode(file_get_contents(CACHE_PATH . "lfw" . DS . "pro_lfw_config_" . sha1($dir) . ".php"), true));
				return;
			}

			$yaml = new \Symfony\Component\Yaml\Yaml();

			$files = array(
				"defaults" => $dir . "config" . DS . "defaults.yml",
				"database" => $dir . "config" . DS . "database.yml",
				"router" => $dir . "config" . DS . "router.yml",
			);

			$settings = array();
			foreach ($files AS $key => $file) {
				$settings[$key] = is_file($file)
					? $yaml->parse(file_get_contents($file))
					: array();
			}

			$current = Settings::get("_lfw_config");
			if (!$current) $current = array();

			foreach ($settings AS $key => $parsed) {
				foreach ($parsed AS $key2 => $configs) {
					$current[$key][$key2] = $configs;
				}
			}

			$settings = $current;

			Settings::set("_lfw_config", $settings);

			if ($this->env == "pro" && !is_file(CACHE_PATH . "lfw" . DS . "prod_lfw_config_" . sha1($dir) . ".php")) {
				file_put_contents(CACHE_PATH . "lfw" . DS . "pro_lfw_config_" . sha1($dir) . ".php", json_encode($settings));
			}
		} catch (Exception $e) {
			throw new Exception\Runtime("Cannot parse defaults.", 0, $e);
		}
	}

	private function setApp() {
		$router = Settings::get("_lfw_config");
		$router = $router["router"];
		$match = null;
		foreach ($router['apps'] AS $app => $config) {
			if (in_array(Context::getRequest()->host(), $config['domains'])) {
				$match = $app;
				break;
			}
		}

		if ($match) {
			$this->app = $app;
			define("APP_PATH", ROOT_PATH . "app" . DS . $app . DS);
			define("APP_SRC_PATH", APP_PATH . "src" . DS);

			$this->parseDefaults(APP_PATH);
		}
	}

	private function setDefaults() {
		$lfwConfig = Settings::get("_lfw_config");
		$appConfig = Settings::get("_lfw_config");

		Settings::set('domain', $appConfig['defaults']['domain']);
		Settings::set('title', $appConfig['defaults']['title']);
		Settings::set('protocol', $appConfig['defaults']['protocol']);
		Settings::set('url', $appConfig['defaults']['protocol'] . "://" . $appConfig['defaults']['domain']);
		Settings::set('hash', $appConfig['defaults']['security']['hash']);
	}

	private function initSession() {
		$SID = session_id(); 
		if(empty($SID)) {
			session_set_cookie_params(7*24*60*60, '/', Settings::get("domain"));
			session_start();
		}
	}

	private function reinitEnvironment($env = null) {
		$this->initEnvironment($env ?: $this->env);
	}

	private function initEnvironment($env = "pro") {
		$this->env = $env;
		if ($env == "dev") {
			error_reporting(E_ALL);
			ini_set("display_errors", E_ALL);
			ini_set("error_log", LOG_PATH . Settings::get("app").".".$env.".log");
		} else {
			error_reporting(NULL);
			ini_set("display_errors", NULL);
			ini_set("error_log", LOG_PATH . Settings::get("app").".".$env.".log");
		}
	}

	private function initDatabase() {
		$config = Settings::get("_lfw_config");
		
		DB::push(new DatabaseInterface\PDO($config['database']['default']));
	}

	public static function db() {
		return DB::inst();
	}

	private function initCache() {
		$config = Settings::get("_app_config");

		new Cache\APC();
	}

	private function buildRouter() {
		$config = Settings::get("_lfw_config");
		$router = $config['router'];

		if (isset($router['resources']))
		foreach ($router['resources'] AS $resource)
			Router::addResource($resource);

		if (isset($router['routes']))
		foreach ($router['routes'] AS $name => $route)
			Router::add($route['url'], $route, $name);
	}

	private function initI18n() {
		$config = Settings::get("_lfw_config");
		if (isset($config['defaults']['i18n'])) {
			$i18n = $config['defaults']['i18n'];
			if (!isset($i18n['type'])) $i18n['type'] = "session";

			$request = Context::getRequest();
			
			if (!isset($i18n['current'])) // because it can be overrided
			foreach ($i18n['langs'] AS $key => $lang) {
				if ($i18n['type'] == "domain" && strpos($request->host(), $lang['code']) === 0) {
					$i18n['current'] = $key;
				} else if ($i18n['type'] == "url" && strpos($request->prodUri(), $lang['code']) === 0) {
					$i18n['current'] = $key;
				} else if ($i18n['type'] == "cookie" 
					&& isset($_COOKIE['lfw'])
					&& ($cookie = unserialize($_COOKIE['lfw']))
					&& isset($cookie['i18n'])
					&& $cookie['i18n'] == $lang['code']) {
					$i18n['current'] = $key;
				}

				if (isset($i18n['current'])) break;
			}

			if (!isset($i18n['current'])) $i18n['current'] = $i18n['default'];

			// set session
			$_SESSION['lfw']['i18n'] = $i18n['current'];

			if ($i18n['force'] == true) // perform redirect
			if ($i18n['type'] == "domain" && strpos($request->host(), $lang['code']) !== 0) {
				Status::redirect($request->scheme() . "://" . $i18n['langs'][$i18n['current']]['code'] . "." . $config['defaults']['domain'] . $request->prodUri());
			} else if ($i18n['type'] == "url" && strpos($request->prodUri(), $lang['code']) !== 0) {
				die("url doesnt work ... yet ... =)");
				Status::redirect($request->scheme() . "://" . $config['defaults']['domain'] . $request->prodUri());
			}
		}
	}

	static function runURL($url = NULL, $data = array()) {
		// get url from router
		if (substr($url, 0, 1) == "@") {
			$url = Router::make(substr($url, 1), $data);
		}

		$configDefaults = Settings::get("_lfw_config");
		try {
			$match = Router::findMatch($url);
		} catch (Exception $e) {
			throw new Exception\Runtime("Cannot find route's match");
		}

		try {
			$layoutData = self::loadLayout($match['layout']);

			// new code parses view into layout (double extending ;-))
			if ($layoutData instanceof View) {
				// parse view into layout
			} else if (is_string($layoutData)) {
				die("why use string as layout");
			} else if (is_null($layoutData) || is_int($layoutData) || is_bool($layoutData)) {
				// without layout
				$layoutData = null;
			} else if (is_array($layoutData)) {
				// send data to view ;-)
			} else {
				throw new Exception\Runtime("Layout is unknown type" . var_dump($layoutData));
			}
		} catch (Exception $e) {
			throw new Exception\Runtime("Cannot load layout " . $match['layout'] . ".", 1, $e);
		}

		try {
			self::loadController($match['controller']);
		} catch (Exception $e) {
			throw new Exception\Runtime("Cannot load controller " . $match['controller'] . ". (" . $e->getMessage() . ")", 1, $e);
		}

		try {
			$viewData = self::loadView($match['view'], array(), self::$controller);

			if ($viewData instanceof View) {
				// parse layout into view
			} else if (is_string($viewData)) {
				// print view as content
				$viewData = array("content" => $viewData);
			} else if (is_null($viewData) || is_int($viewData) || is_bool($viewData)) {
				// without view
				$viewData = null;
			} else if (is_array($viewData)) {
				// send data to layout ;-)
			} else {
				throw new Exception\Runtime("view is unknown type" . var_dump($viewData));
			}
		} catch (Exception $e) {
			throw new Exception\Runtime("Cannot load view " . $match['controller'] . ":" . $match['view'] . " (".$e->getMessage().")", 1, $e);
		}
		
		$controllerArrData = self::$controller->__toArray();
		$layoutArrData = self::$layout instanceof Controller
			? self::$layout->__toArray()
			: array();

		if (!$viewData && !$layoutData && !$controllerArrData) {
			throw new Exception\Runtime("Seriously, without data?");
		}

		if ($viewData instanceof View) {
			if (is_array($layoutArrData)) $viewData->addData($layoutArrData);
			if (is_array($controllerArrData)) $viewData->addData($controllerArrData);
			if (is_array($layoutData)) $viewData->addData($layoutData);
		}

		if ($layoutData instanceof View) {
			if (is_array($layoutArrData)) $layoutData->addData($layoutArrData);
			if (is_array($controllerArrData)) $layoutData->addData($controllerArrData);
			if (is_array($viewData)) $layoutData->addData($viewData);
		}

		$response = Context::getResponse();

		// parse view into layout
		if ($viewData instanceof View && $layoutData instanceof View) {
			$viewData = array("content" => $viewData->autoparse());
			$layoutData->addData($viewData);
			$response->setOutput($layoutData->autoparse());
		} else if ($viewData instanceof View) {
			$response->setOutput($viewData->autoparse());
		} else if ($layoutData instanceof View) {
			$response->setOutput($layoutData->autoparse());
		} else {
			if (!$layoutData) {
				if (is_string($viewData)) {
					$response->setOutput($viewData);
				} else if (is_array($viewData)) {
					if (isset($viewData["content"])) {
						$response->setOutput($viewData["content"]);
					} else {
						var_dump($layoutData, $viewData);
						die("no data?");
					}
				} else {
					var_dump($viewData);
					die("what is view data?");
				}
			} else {
				var_dump($layoutData, $viewData);
				die("no layout data?");
			}
		}

		$response->output();
	}

	public static function getCore() {
		return self::$core;
	}

	private static function loadLayout($layout) {
		if (!$layout)
			return TRUE;

		$explode = explode("\\", $layout);

		$releaser = $explode[0];

		$layout = explode(":", $explode[1]);
		$layout[1] = isset($layout[1]) ? $layout[1] : $layout[0];

		$c = "\\" . $releaser . '\\' . Core::toCamel($layout[0]) . '\Controller\\' . Core::toCamel($layout[0]);
		$c2 = "\\" . self::$core->app . $c;

		if (class_exists($c2)) $c = $c2;

		self::$layout = self::reflect($c);

		return self::reflect(self::$layout, lcfirst($layout[1]) . "Action");
	}

	private static function loadLayoutAfter($layout) {
		if (!self::$layout)
			return TRUE;

		$explode = explode("\\", $layout);

		$releaser = $explode[0];

		$layout = explode(":", $explode[1]);
		$layout[1] = isset($layout[1]) ? $layout[1] : $layout[0];

		if (method_exists(self::$layout, lcfirst($layout[1]) . "AfterAction"))
			return self::reflect(self::$layout, lcfirst($layout[1]) . "AfterAction");
	}

	public static function loadController($controller) {
		$controller = explode(":", $controller);
		$controller[1] = isset($controller[1]) ? $controller[1] : substr(strrchr($controller[0], "\\"), 1);

		$c = '\\' . Core::toCamel($controller[0]) . '\Controller\\' . Core::toCamel($controller[1]);
		$c2 = "\\" . self::$core->app . $c;

		if (class_exists($c2)) $c = $c2;

		$reflect = self::reflect($c);

		if (!self::$controller || get_class(self::$controller) != get_class($reflect)) self::$controller = $reflect;
		
		return $reflect;
	}

	public static function loadView($view, $data = array(), $controller = NULL) {
		if (!$controller) $controller = self::$controller;

		if (method_exists($controller, $view . "Prepare"))
			self::reflect($controller, $view . "Prepare", $data, get_class($controller));

		if (method_exists($controller, $view . "Action")) {
			return self::reflect($controller, $view . "Action", $data, get_class($controller));
		} else
			throw new Exception("Method " . $view . "Action() in controller " . get_class($controller) . " doesn't exist.");

		return FALSE;
	}

	public static function reflect($class, $method = "__construct", $data = array(), $lazyLoad = FALSE) {
		try {
			$r = new \ReflectionMethod($class, $method);

			$params = $r->getParameters();
			$arrParams = array();
			foreach ($params as $key => $param) {
				if (array_key_exists($param->name, $data)) {
					$arrParams[] = $data[$param->name];
					continue;
				}
				
				if ($param->getDefaultValue()) {
					$val = $param->getDefaultValue();
				} else if ($param->allowsNull() || $param->isOptional()) {
					$val = NULL;
				} else {
					throw new Exception\Runtime("Cannot find value for parameter " . $key . " in class " . $class);
				}

				$arrParams[] = $val;
			}

			if ($method == "__construct") {
				$reflect  = new \ReflectionClass($class);
				return $reflect->newInstanceArgs($arrParams);
			} else {
				return call_user_func_array(array($class, $method),  $arrParams);
			}
		} catch (Exception $e) {
			throw new Exception\Runtime("Cannot reflect class " . (is_string($class) ? $class : get_class($class)) . ":" . $method, 1, $e);
		}
	}

	public static function getContext() {
		return self::$context;
	}

	public static function toCamel($text) {
		$text = str_split($text, 1);

		foreach ($text AS $index => $char) {
			if (($char == "_" && isset($text[$index + 1]))
				|| ($char == "\\" && isset($text[$index + 1]))) {
				$text[$index + 1] = mb_strtoupper($text[$index + 1]);
			}
		}

		return ucfirst(str_replace("_", "", implode($text)));
	}

	public static function fromCamel($text) {
		$return = NULL;
		$text = str_split($text, 1);

		foreach ($text AS $index => $char) {
			if ($char != strtolower($char) && $index != 0 && (isset($text[$index-1]) && $text[$index-1] != "/" && $text[$index-1] != "\\")) {
				$return .= "_";
			}

			$return .= $char;
		}

		return strtolower($return);
	}
}

}

namespace {
	function __($key, $lang = NULL) {
		return LFW\Lang::get($key, $lang);
	}
}

?>