<?php

namespace LFW\Traits;

use LFW\Context;

trait Get {
  public function getView($view, $controller, $data) {
    return Core::loadView($view, $data, $this);
  }

  public function getEntity($entity = NULL) {
    return Context::getEntity($entity);
  }

  public function getController($controller = NULL) {
    return Context::getController($controller);
  }

  public function getRecord($record = NULL) {
    return Context::getRecord($record);
  }

  public function getForm($form = NULL) {
    return Context::getForm($form);
  }

  public function getRequest() {
    return Context::getRequest();
  }

  public function getSession() {
    return Context::getRequest()->getSession();
  }

  public function getGet() {
    return self::getRequest()->getGet();
  }

  public function getPost() {
    return self::getRequest()->getPost();
  }

  public function getCore() {
    return Context::getCore();
  }

  public function getRouter() {
    return Context::getRouter();
  }

  public function getDB() {
    return Context::getDB();
  }
}

?>