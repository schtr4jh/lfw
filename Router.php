<?php

namespace LFW;

class Router {
	private static $routes = array();
	private static $prefix;

	private static $layout;
	private static $controller;
	private static $view;
	private static $data;

	public function __call($method, $args = array())
    {
      if(is_callable(array($this, $method))) {
      	echo $method;
          return call_user_func_array($this->$method, $args);
      }
    }

	public static function setPrefix($prefix = NULL) {
		self::$prefix = $prefix;
	}

	public static function getPrefix() {
		return self::$prefix;
	}

	public static function getRoutes() {
		return self::$routes;
	}

	public static function firendly($string, $maxLength = 80, $separator = '-') {
		$url = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $string);
		$url = preg_replace('/[^a-zA-Z0-9 -]/', '', $url);
		$url = trim(substr(strtolower($url), 0, $maxLength));
		$url = preg_replace('/[s' . $separator . ']+/', $separator, $url);
		
		return $url;
	}

	public static function add($route, $conf = array(), $name = NULL) {
		$conf["name"] = $name;
		$conf["url"] = $route;

		if (!isset(self::$routes[$conf["url"]])) self::$routes[$conf["url"]] = array();

		array_unshift(self::$routes[$conf["url"]], $conf);
	}

	private static function parseYML($file) {
		$yaml = new \Symfony\Component\Yaml\Yaml();

		$router = $yaml->parse(file_get_contents($file));

		if (isset($router['resources']))
		foreach ($router['resources'] AS $resource)
			self::addResource($resource);

		if (isset($router['routes']))
		foreach ($router['routes'] AS $name => $route)
			self::add($route['url'], $route, $name);
	}

	public static function addResource($module) {
		if (is_file(SRC_PATH . str_replace("\\", DS, $module) . DS . "config" . DS . "router.yml")) {
			try {
				self::parseYML(SRC_PATH . str_replace("\\", DS, $module) . DS . "config" . DS . "router.yml");
			} catch (Exception $e) {
				die($e->getMsg());
				Debug::addError($e->getMsg());
			}
		}
	}

	public static function make($routeName, $arguments = array(), $absolute = false) { // @ToDo
		foreach (self::$routes AS $routeArr)
			foreach ($routeArr AS $route)
				if ($route["name"] == $routeName) {
					$args = array();
					foreach ($arguments AS $key => $val) {
						$args["[".$key."]"] = $val;
					}
					if ($args)
						$route['url'] = str_replace(array_keys($args), $args, $route['url']);

					return ($absolute ? Settings::get("url") : "") . (Settings::get("dev") ? "/dev.php" : "") . $route["url"];
				}
	}

	public static function get($param) {
		return isset(self::$data[$param]) ? self::$data[$param] : NULL;
	}

	public static function getUri() {
		return $_SERVER['REQUEST_URI'];
	}

	public static function getURL() {
		return self::getUri();
	}

	public static function findMatch($url = NULL) {
		if (empty($url)) {
			$url = $_SERVER['REQUEST_URI'];
		}

		if (strpos($url, "/dev.php") === 0)
			$url = substr($url, 8);

		// remove ending slash
		if (strlen($url) > 1 && substr($url, -1) == "/")
			$url = substr($url, 0, -1);

		// exact match
		$found = FALSE;
		$match = FALSE;
		foreach (self::$routes AS $routeArr) {
			foreach ($routeArr AS $route) {
				if ($route["url"] == $url && !(strpos($url, "[") || strpos($url, "]"))) {
					// validate method
					if (isset($route['method']) && !empty($route['method']) && !in_array(strtolower($_SERVER['REQUEST_METHOD']), explode("|", $route['method']))) {
						break;
					}
					
					// validate secure
					if (isset($route['secure']) && is_callable($route['secure']) && !$route['secure']()) {
						break;
					}

					$found = TRUE;
					$match = $route;
					break;
				}
			}
		}

		if (!$found) {
			$arrUrl = explode("/", substr($url, 1));
			foreach (self::$routes AS $routeArr) {
				foreach ($routeArr AS $conf) {
					$arrRoutes = explode("/", substr($conf["url"], 1));

					// check only urls longer than routes
					if (count($arrRoutes) > count($arrUrl)) {
						continue;
					}

					// validate method
					if (isset($conf['method']) && !empty($conf['method']) && !in_array(strtolower($_SERVER['REQUEST_METHOD']), explode("|", $conf['method']))) {
						continue;
					}

					// validate secure
					if (isset($conf['secure']) && is_callable($conf['secure']) && !$conf['secure']()) {
						continue;
					}

					$error = FALSE;
					$regexData = array();
					for ($i = 0; $i < count($arrUrl); $i++) {
						if (!isset($arrRoutes[$i])) {
							if ($arrRoutes[$i-1] == "*") {
								// ok
								break;
							} else {
								$error = TRUE;
								break;
							}
						} else if ($arrRoutes[$i] == $arrUrl[$i]) {
							// ok
							continue;
						} else if (substr($arrRoutes[$i], 0, 1) == "[" && substr($arrRoutes[$i], -1) == "]") {
							$var = substr($arrRoutes[$i], 1, -1);

							// validate url parts
							if (isset($conf["validate"][$var])) {
								if (is_callable($conf["validate"][$var])) {
									if ($conf["validate"][$var]($arrUrl[$i]) == TRUE) {
										$regexData[$var] = $arrUrl[$i];
										// ok
									} else {
										$error = TRUE;
										break;
									}
								} else if ($conf["validate"][$var] == "int") {
									if (Validate::isInt($arrUrl[$i])) {
										$regexData[$var] = $arrUrl[$i];
										// ok
									} else {
										$error = TRUE;
										break;
									}
								} else if ($conf["validate"][$var] == "string") {
									if (Validate::isString($arrUrl[$i])) {
										$regexData[$var] = $arrUrl[$i];
										// ok
									} else {
										$error = TRUE;
										break;
									}
								} else if ($var == "id") {
									if (Validate::isInt($arrUrl[$i]) && $arrUrl[$i] > 0) {
										$regexData[$var] = $arrUrl[$i];
										// ok
									} else {
										$error = TRUE;
										break;
									}
								} else if (is_array($conf["validate"][$var]) && in_array($arrUrl[$i], $conf["validate"][$var])) {
									$regexData[$var] = $arrUrl[$i];
									// ok
								} else if (is_string($conf["validate"][$var]) && preg_match($conf["validate"][$var], $arrUrl[$i])) {
									$regexData[$var] = $arrUrl[$i];
									// ok
								} else {
									$error = TRUE;
									break;
								}
							} else {
								$regexData[$var] = $arrUrl[$i];
								// ok
							}
						} else if ($arrRoutes[$i] == "*") {
							// ok
						} else {
							$error = TRUE;
							break;
						}

						if ($error == TRUE) {
							break;
						}
					}

					if ($error == FALSE) {
						$match = $conf;
						$match["data"] = $regexData;
						$found = TRUE;
						break;
					}
				}

				if ($found) {
					break;
				}
			}
		}
			
		if (!$found) {
			throw new \Exception("Routing match not found - " . $url);
		}

		if (isset($match["data"]["controller"]))
			$match['controller'] = $match["data"]["controller"];

		if (isset($match["data"]["layout"]))
			$match['layout'] = $match["data"]["layout"];

		if (isset($match["data"]["view"]))
			$match['view'] = $match["data"]["view"];

		if (isset($match["controller"]) && isset($match["transform"]["controller"])) {
			if (is_callable($match["transform"]["controller"])) {
				$match["controller"] = $match["transform"]["controller"]($match["controller"]);
			} else if (is_array($match["transform"]["controller"])) {
				if (isset($match["transform"]["controller"]["prefix"]))
					$match["controller"] = $match["transform"]["controller"]["prefix"] . $match["controller"];
			}
		}
		
		// do we really need it?
		if (!isset($match["layout"])) {
			$match["layout"] = null;
		}

		if (!isset($match["controller"]))
			throw new \Exception("Controller not set.");

		if (!isset($match["view"]))
			throw new \Exception("View not set.");

		self::$layout = $match['layout'];
		self::$controller = $match['controller'];
		self::$view = $match['view'];
		self::$data = isset($match['data']) ? $match['data'] : array();

		self::$data["layout"] = self::$layout;
		self::$data["controller"] = self::$controller;
		self::$data["view"] = self::$view;
		self::$data["name"] = $match['name'];
		self::$data["url"] = $match['url'];

		return $match;
	}
}

?>